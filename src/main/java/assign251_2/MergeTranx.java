package assign251_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MergeTranx {


	//create two loggers -- mergetranx is parent so mergetranx.file can inherit from
	static Logger tranxlog = Logger.getLogger("mergetranx");	
	static Logger fileLog = Logger.getLogger("mergetranx.file");	

	public static void run() throws IOException {
		
		NumberFormat CURRENCY_FORMAT = NumberFormat.getCurrencyInstance(Locale.getDefault());	
		
		//configure filelog so that it does not use translog's assigned level
		BasicConfigurator.configure();
		tranxlog.setLevel(Level.INFO);
		fileLog.setLevel(Level.DEBUG);
		//fileLog.setAdditivity(false);
		
		List<Purchase> transactions = new ArrayList<Purchase>();
		
		// read data from 4 files
		readData("src/main/resources/transactions1.csv",transactions);
		readData("src/main/resources/transactions2.csv",transactions);
		
		// display on console values to Log some info for the user
		tranxlog.info("" + transactions.size() + " transactions imported");
		tranxlog.info("total value: " + CURRENCY_FORMAT.format(computeTotalValue(transactions)));
		tranxlog.info("max value: " + CURRENCY_FORMAT.format(computeMaxValue(transactions)));
	}
	
	private static double computeTotalValue(List<Purchase> transactions) {
		double v = 0.0;
		for (Purchase p:transactions) {
			v = v + p.getAmount();
		}
		return v;
	}
	
	private static double computeMaxValue(List<Purchase> transactions) {
		double v = 0.0;
		for (Purchase p:transactions) {
			v = Math.max(v,p.getAmount());
		}
		return v;
	}

	// read transactions from a file, and add them to a list
	private static void readData(String fileName, List<Purchase> transactions)  {
		
		DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			//set the configuration so filelog also outputs to a txt file
			fileLog.addAppender (
					new org.apache.log4j.FileAppender(
							new org.apache.log4j.SimpleLayout(), "logs.txt"	
						)
					);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		File file = new File(fileName);
		String line = null;
		// print info for user
		fileLog.info("import data from " + fileName);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine())!=null) {
				String[] values = line.split(",");
				Purchase purchase = new Purchase(
						values[0],
						Double.parseDouble(values[1]),
						DATE_FORMAT.parse(values[2])
				);
				transactions.add(purchase);
				// this is for debugging only
				fileLog.debug("imported transaction " + purchase);
			} 
		}
		catch (FileNotFoundException x) {
			// print warning to console and to a file
			x.printStackTrace();
			fileLog.warn("file " + fileName + " does not exist - skip");
		}
		catch (IOException x) {
			// print error message and details
			x.printStackTrace();
			fileLog.error("problem reading file " + fileName);
		}
		// happens if date parsing fails
		catch (ParseException x) { 
			// print error message and details
			x.printStackTrace();
			fileLog.error("cannot parse date from string - please check whether syntax is correct: " + line);	
		}
		// happens if double parsing fails
		catch (NumberFormatException x) {
			// print error message and details
			fileLog.error("cannot parse double from string - please check whether syntax is correct: " + line);	
		}
		catch (Exception x) {
			// any other exception 
			// print error message and details
			fileLog.error("exception reading data from file " + fileName + ", line: " + line);	
		}
		finally {
			try {
				if (reader!=null) {
					reader.close();
				}
			} catch (IOException e) {
				// print error message and details
				fileLog.error("cannot close reader used to access " + fileName);
			}
		}
	}

}
